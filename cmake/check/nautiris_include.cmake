include_guard()

set(NAUTIRIS_INCLUDES)
macro(nautiris_check_include_file header var)
  check_include_files("${NAUTIRIS_INCLUDES};${header}" ${var})
  if(${var})
    set(NAUTIRIS_INCLUDES ${NAUTIRIS_INCLUDES} ${header})
  endif()
endmacro()

nautiris_check_include_file("inttypes.h" HAVE_INTTYPES_H)
nautiris_check_include_file("stdint.h" HAVE_STDINT_H)
nautiris_check_include_file("stdlib.h" HAVE_STDLIB_H)
nautiris_check_include_file("string.h" HAVE_STRING_H)
nautiris_check_include_file("strings.h" HAVE_STRINGS_H)
nautiris_check_include_file("unistd.h" HAVE_UNISTD_H)

set(NAUTIRIS_INCLUDE_FILES)
foreach(it ${NAUTIRIS_INCLUDES})
  set(NAUTIRIS_INCLUDE_FILES "${NAUTIRIS_INCLUDE_FILES}#include <${it}>\n")
endforeach()

include(CheckCSourceCompiles)
check_c_source_compiles(
  "#define __EXTENSIONS__ 1
   ${NAUTIRIS_INCLUDE_FILES}
   int main() { return 0; }"
  SAFE_TO_DEFINE_EXTENSIONS)

unset(NAUTIRIS_INCLUDE_FILES)
unset(NAUTIRIS_INCLUDES)
