include_guard()

function(nautiris_enable_ccache)
  if(NOT NAUTIRIS_ENABLE_CACHE)
    return()
  endif()

  find_program(CCACHE_EXE ccache)
  if(NOT CCACHE_EXE)
    message(${NAUTIRIS_MESSAGE_WARNING} "ccache requested but executable not found.")
    return()
  endif()

  set(CMAKE_C_COMPILER_LAUNCHER ${CCACHE_EXE} PARENT_SCOPE)
  set(CMAKE_CXX_COMPILER_LAUNCHER ${CCACHE_EXE} PARENT_SCOPE)
endfunction()
