include_guard()

list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/definition")

include(nautiris_defvar)
include(nautiris_option)
include(nautiris_override)
include(nautiris_policy)

list(REMOVE_AT CMAKE_MODULE_PATH -1)
