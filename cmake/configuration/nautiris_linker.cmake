include_guard()

function(nautiris_target_ipo TARGET_NAME)
  if(NOT NAUTIRIS_ENABLE_IPO)
    return()
  endif()
  include(CheckIPOSupported)
  check_ipo_supported(RESULT result OUTPUT output)
  if(result)
    set_target_properties(${TARGET_NAME} PROPERTIES
      INTERPROCEDURAL_OPTIMIZATION ON)
  else()
    message(${NAUTIRIS_MESSAGE_WARNING} "IPO is not supported: ${output}.")
  endif()
endfunction()

if(NAUTIRIS_COMPILER_IS_MSVC)
else()
  list(APPEND NAUTIRIS_LINK_OPTIONS_COMMON
    $<$<BOOL:${NAUTIRIS_USE_LINKER}>:-fuse-ld=${NAUTIRIS_USE_LINKER}>
    )
  if(NAUTIRIS_COMPILER_IS_GCC)
    if(NAUTIRIS_USE_STDLIB)
      list(APPEND NAUTIRIS_LINK_OPTIONS_COMMON
        -nodefaultlibs
        -lm
        -lc
        $<$<PLATFORM_ID:Linux>:-lgcc_s\;-lgcc>
        $<IF:$<AND:$<BOOL:${NAUTIRIS_TARGET_IS_32BIT}>,$<NOT:$<BOOL:${NAUTIRIS_ARCH_IS_32BIT}>>>,-m32,-march=native>
        $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<STREQUAL:${NAUTIRIS_USE_STDLIB},cxx>>:-lc++\;-lc++abi\;-lsupc++>
        $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<STREQUAL:${NAUTIRIS_USE_STDLIB},stdcxx>>:-lstdc++>
      )
    endif()
    list(APPEND NAUTIRIS_LINK_OPTIONS_DEVELOP
      # $<$<BOOL:${NAUTIRIS_ENABLE_COVERAGE}>:-fprofile-arcs\;-ftest-coverage>
      )
  elseif(NAUTIRIS_COMPILER_IS_CLANG)
    if(NAUTIRIS_USE_STDLIB)
      list(APPEND NAUTIRIS_LINK_OPTIONS_COMMON
        -stdlib=libc++
        -lc++
      )
    endif()
    list(APPEND NAUTIRIS_LINK_OPTIONS_DEVELOP
      # $<$<BOOL:${NAUTIRIS_ENABLE_COVERAGE}>:-fprofile-instr-generate\;-fcoverage-mapping>
      )
  else()
    message(${NAUTIRIS_MESSAGE_WARNING} "Unknow compiler ${CMAKE_CXX_COMPILER_ID}.")
  endif()
endif()
