include_guard()

if(CMAKE_GENERATOR MATCHES "Ninja")
  if(NAUTIRIS_COMPILER_IS_MSVC)
  elseif(NAUTIRIS_COMPILER_IS_GCC)
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
      -fdiagnostics-color=always
      )
  elseif(NAUTIRIS_COMPILER_IS_CLANG)
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
      -fcolor-diagnostics
      )
  endif()
endif()
