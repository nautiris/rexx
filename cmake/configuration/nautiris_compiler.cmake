include_guard()

if(NAUTIRIS_COMPILER_IS_MSVC)
  list(APPEND NAUTIRIS_COMPILE_DEFINITIONS_COMMON
    _CRT_NONSTDC_NO_DEPRECATE
    _CRT_SECURE_NO_DEPRECATE
    _CRT_SECURE_NO_WARNINGS
    _SCL_SECURE_NO_WARNINGS
    NOMINMAX # disable min/max problem in windows.h
    )
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
    /bigobj # Compiler Fatal error C1128: Object Limit Exceed
    /Oi # enable built-in functions
    /permissive-
    /source-charset:utf-8
    /volatile:iso # Specifies how the volatile keyword is to be interpreted.
    /wd4996 # mark deprecated (/W3)
    $<$<COMPILE_LANGUAGE:CXX>:/GR-> # Disable RTTI
    $<$<AND:$<BOOL:${NAUTIRIS_WARNINGS_AS_ERRORS}>,$<VERSION_GREATER:MSVC_VERSION,1900>>:/WX>
    )
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
    /Wall
    )
else()
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
    -fno-common
    $<IF:$<AND:$<BOOL:${NAUTIRIS_TARGET_IS_32BIT}>,$<NOT:$<BOOL:${NAUTIRIS_ARCH_IS_32BIT}>>>,-m32,-march=native>
    $<$<BOOL:${NAUTIRIS_WARNINGS_AS_ERRORS}>:-Werror\;-Wno-error=deprecated-declarations>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti> # Disable RTTI
    )
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
    -Wall
    -fno-inline
    -pedantic
    # from lua Makefile, enable compiler warning
    -Wdisabled-optimization
    -Wdouble-promotion
    -Wextra
    -Wmissing-declarations
    -Wredundant-decls
    -Wshadow
    -Wsign-compare
    -Wundef
    # -Wfatal-errors
    # from kDE cmake
    -Wcast-align
    -Wno-long-long
    )
  if(NAUTIRIS_COMPILER_IS_GCC)
    # GNU Compiler Collection
    # Learn more at https://gcc.gnu.org/onlinedocs/gcc/Invoking-GCC.html
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
      -fmax-errors=5
      -finput-charset=UTF8
      $<$<COMPILE_LANGUAGE:CXX>:-fno-threadsafe-statics\;-fmerge-all-constants>
      # $<$<BOOL:${NAUTIRIS_ENABLE_COVERAGE}>:-fprofile-arcs\;-ftest-coverage>
      $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<NOT:$<STREQUAL:${NAUTIRIS_USE_STDLIB},>>>:-nostdinc++>
      )
    foreach(dir IN LISTS NAUTIRIS_STDLIB_INCLUDEDIRS)
      list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON -isystem${dir})
    endforeach()
  elseif(NAUTIRIS_COMPILER_IS_CLANG)
    # Clang
    # Learn more at https://clang.llvm.org/docs/UsersManual.html
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_COMMON
      -ferror-limit=5
      -finput-charset=UTF-8
      $<$<COMPILE_LANGUAGE:CXX>:-Wthread-safety>
      # $<$<BOOL:${NAUTIRIS_ENABLE_COVERAGE}>:-fprofile-instr-generate\;-fcoverage-mapping>
      $<$<BOOL:$<STREQUAL:${NAUTIRIS_USE_STDLIB},cxx>>:-stdlib=libc++>
      )
  else()
    message(${NAUTIRIS_MESSAGE_WARNING} "Unknow compiler ${CMAKE_CXX_COMPILER_ID}.")
  endif()
endif()
