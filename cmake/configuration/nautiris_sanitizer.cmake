include_guard()

set(NAUTIRIS_ADDED_SANITIZER OFF)
foreach(sanitizer IN LISTS NAUTIRIS_USE_SANITIZER)
  if(sanitizer STREQUAL "Address")
    # Learn more at https://github.com/google/sanitizers/wiki/AddressSanitizer
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
      $<IF:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>,/fsanitize=address,-fsanitize=address\;-fsanitize-address-use-after-scope>)
    list(APPEND NAUTIRIS_LINK_OPTIONS_DEVELOP
      $<$<NOT:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>>:-lasan>
      $<IF:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>,/fsanitize=address,-fsanitize=address>)
    set(NAUTIRIS_ADDED_SANITIZER ON)
  elseif(sanitizer MATCHES "Memory(WithOrigins)?")
    # Learn more at https://github.com/google/sanitizers/wiki/MemorySanitizer
    if("Address" IN_LIST NAUTIRIS_USE_SANITIZER OR
        "Leak" IN_LIST NAUTIRIS_USE_SANITIZER OR
        "Thread" IN_LIST NAUTIRIS_USE_SANITIZER)
      message(${NAUTIRIS_MESSAGE_WARNING}
        "Memory sanitizer does not work with Address, Leak and Thread sanitizer enabled")
    elseif(NOT NAUTIRIS_COMPILER_IS_GCC)
      list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
        $<IF:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>,/fsanitize=memory,-fsanitize=memory>)
      list(APPEND NAUTIRIS_LINK_OPTIONS_DEVELOP
        $<IF:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>,/fsanitize=memory,-fsanitize=memory>)
      if(NAUTIRIS_COMPILER_IS_CLANG AND sanitizer STREQUAL "MemoryWithOrigins")
        list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP -fsanitize-memory-track-origins)
        list(APPEND NAUTIRIS_LINK_OPTIONS_DEVELOP -fsanitize-memory-track-origins)
      endif()
      set(NAUTIRIS_ADDED_SANITIZER ON)
    endif()
  elseif(sanitizer MATCHES "Undefined")
    # Learn more at https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
      $<IF:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>,/fsanitize=undefined,-fsanitize=undefined>
      $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<NOT:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>>>:-fno-sanitize=vptr>)
    list(APPEND NAUTIRIS_LINK_OPTIONS_DEVELOP
      $<$<NOT:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>>:-lubsan>
      $<IF:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>,/fsanitize=undefined,-fsanitize=undefined>)
    set(NAUTIRIS_ADDED_SANITIZER ON)
  elseif(sanitizer MATCHES "Thread")
    # Learn more at https://github.com/google/sanitizers/wiki/ThreadSanitizerCppManual
    if("Address" IN_LIST NAUTIRIS_USE_SANITIZER OR
        "Leak" IN_LIST NAUTIRIS_USE_SANITIZER OR
        "Memory" IN_LIST NAUTIRIS_USE_SANITIZER OR
        "MemoryWithOrigins" IN_LIST NAUTIRIS_USE_SANITIZER)
      message(${NAUTIRIS_CMAKE_MESSAGE_WARNING}
        "Thread sanitizer does not work with Address, Leak and Memory sanitizer enabled")
    else()
      list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
        $<IF:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>,/fsanitize=thread\;/O1,-fsanitize=thread\;-O1>)
      list(APPEND NAUTIRIS_LINK_OPTIONS_DEVELOP
        $<$<NOT:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>>:-ltsan>
        $<IF:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>,/fsanitize=thread,-fsanitize=thread>)
      set(NAUTIRIS_ADDED_SANITIZER ON)
    endif()
  elseif(sanitizer MATCHES "Leak")
    # Learn more at https://github.com/google/sanitizers/wiki/AddressSanitizerLeakSanitizer
    list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP
      $<IF:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>,/fsanitize=leak,-fsanitize=leak>)
    list(APPEND NAUTIRIS_LINK_OPTIONS_DEVELOP
      $<$<NOT:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>>:-llsan>
      $<IF:$<BOOL:${NAUTIRIS_COMPILER_IS_MSVC}>,/fsanitize=leak,-fsanitize=leak>)
    set(NAUTIRIS_ADDED_SANITIZER ON)
  else()
    message(${NAUTIRIS_MESSAGE_WARNING}
      "This sanitizer not yet supported in the C / C++ environment: ${sanitizer}")
  endif()
endforeach()

if(NOT NAUTIRIS_COMPILER_IS_MSVC AND NAUTIRIS_ADDED_SANITIZER)
  list(APPEND NAUTIRIS_COMPILE_OPTIONS_DEVELOP -fno-omit-frame-pointer)
endif()
unset(NAUTIRIS_ADDED_SANITIZER)
