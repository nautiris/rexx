include_guard()

include(nautiris_check)
include(nautiris_definition)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib")  # static link library files
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib")  # dynamic link library files
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin")  # excutable files

list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/configuration")

include(nautiris_compiler)
include(nautiris_generator)
include(nautiris_linker)
include(nautiris_precompile_header)

list(REMOVE_AT CMAKE_MODULE_PATH -1)
