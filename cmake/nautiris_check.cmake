include_guard()

include(nautiris_definition)

include(CheckFunctionExists)
include(CheckIncludeFile)
include(CheckIncludeFiles)
include(CheckLibraryExists)
include(CheckStructHasMember)
include(CheckSymbolExists)
include(CheckTypeSize)
include(CMakePushCheckState)

list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/check")

include(nautiris_include)
include(nautiris_number)

list(REMOVE_AT CMAKE_MODULE_PATH -1)
