include_guard()

include(nautiris_check)
include(nautiris_definition)

list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/packman")

list(REMOVE_AT CMAKE_MODULE_PATH -1)
